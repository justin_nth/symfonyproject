<?php

namespace App\Command;

use App\Entity\Categorie;
use App\Entity\Equipement;
use App\Entity\Theme;
use App\Entity\Type;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

class ImportCsvCommand extends Command
{
    protected static $defaultName = 'app:import-csv';
    private $em;
    private $csvParsingOptions = [
        'finder_in' => 'data/',
        'finder_name' => 'equipements.csv',
        'ignoreFirstLine' => true,
    ];

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct(self::$defaultName);
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null); // <=== Astuce
    }

    protected function configure()
    {
        $this
            ->setDescription('Import données equipements culturels')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $csv = $this->parseCSV();

        return 0;
    }

    /**
     * Parse a csv file.
     *
     * @return string
     */
    private function parseCSV()
    {
        $ignoreFirstLine = $this->csvParsingOptions['ignoreFirstLine'];

        $finder = new Finder();
        $finder->files()
            ->in($this->csvParsingOptions['finder_in'])
            ->name($this->csvParsingOptions['finder_name'])
        ;

        foreach ($finder as $file) {
            $csv = $file;
        }

        $rows = [];
        if (false !== ($handle = fopen($csv->getRealPath(), 'r'))) {
            $i = 0;
            while (false !== ($data = fgetcsv($handle, null, ';'))) {
                ++$i;
                if ($ignoreFirstLine && 1 === $i) {
                    continue;
                }

                // Create a theme
                $themeRepo = $this->em->getRepository(Theme::class);
                if (!empty($data[3])) {
                    $theme = $themeRepo->findOneBy(['name' => $data[3]]);
                    if (null === $theme) {
                        $theme = new Theme();
                        $theme->setName($data[3]);
                        $this->em->persist($theme);
                        $this->em->flush();
                    }
                } else {
                    $theme = null;
                }

                // Create a category
                $categoryRepo = $this->em->getRepository(Categorie::class);
                if (!empty($data[5])) {
                    $category = $categoryRepo->findOneBy(['name' => $data[5]]);
                    if (null === $category) {
                        $category = new Categorie();
                        $category->setName($data[5]);
                        $this->em->persist($category);
                        $this->em->flush();
                    }
                } else {
                    $category = null;
                }

                // Create a type
                $typeRepo = $this->em->getRepository(Type::class);
                if (!empty($data[7])) {
                    $type = $typeRepo->findOneBy(['name' => $data[7]]);
                    if (null === $type) {
                        $type = new Type();
                        $type->setName($data[7]);
                        $this->em->persist($type);
                        $this->em->flush();
                    }
                } else {
                    $type = null;
                }

                // Create new equipment
                $equipment = new Equipement();
                $equipment->setName($data[1]);
                $equipment->setStatut($data[8]);
                $equipment->setCommune($data[9]);
                $equipment->setAdresse($data[10]);
                $equipment->setTelephone((int) ($data[11]));
                $equipment->setWeb($data[12]);
                $equipment->setCodePostal($data[13]);
                $equipment->setTheme($theme);
                $equipment->setCategorie($category);
                $equipment->setType($type);
                $this->em->persist($equipment);
            }
            fclose($handle);
        }

        $this->em->flush();
    }
}
